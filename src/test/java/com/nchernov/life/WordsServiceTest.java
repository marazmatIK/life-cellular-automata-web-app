package com.nchernov.life;

import com.nchernov.life.entity.Word;
import com.nchernov.life.exceptions.DictionaryException;
import com.nchernov.life.exceptions.WordNotFoundException;
import com.nchernov.life.services.EntityDAO;
import com.nchernov.life.services.WordsService;
import junit.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by zugzug on 28.09.15.
 */
public class WordsServiceTest {

    private static EntityDAO<Word> mockDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> mockEmptyDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> singleItemDAO = mock(EntityDAO.class);
    private static EntityDAO<Word> pairItemsDAO = mock(EntityDAO.class);
    static {
        when(mockDAO.find(any(String.class))).thenReturn(Arrays.asList(new Word(1L, "House"), new Word(2L, "MD")));
        when(singleItemDAO.find(any(String.class))).thenReturn(Arrays.asList(new Word(1L, "House")));
        when(pairItemsDAO.find(contains("House"))).thenReturn(Arrays.asList(new Word(1L, "House")));
        when(pairItemsDAO.find(contains("Tree"))).thenReturn(Arrays.asList(new Word(2L, "Tree")));
        when(pairItemsDAO.find(contains("1"))).thenReturn(Arrays.asList(new Word(1L, "House")));
        when(pairItemsDAO.find(contains("2"))).thenReturn(Arrays.asList(new Word(2L, "Tree")));
    }

    @Test
    public void getById() {
        WordsService wordsService = new WordsService(mockDAO);
        wordsService.getWord(1L);
    }

    @Test
    public void saveWord() {
        WordsService wordsService = new WordsService(mockEmptyDAO);
        Word word = new Word();
        word.setValue("Cat");
        try {
            wordsService.addWordEntity(word, false);
        } catch (Exception ex) {
            Assert.fail("Operation failed");
        }
    }

    @Test(expected = DictionaryException.class)
    public void saveDuplicateWord() {
        WordsService wordsService = new WordsService(singleItemDAO);
        Word word = new Word();
        word.setValue("House");
        wordsService.addWordEntity(word, false);
    }

    @Test(expected = DictionaryException.class)
    public void editWordToBeDuplicateOfOther() {
        WordsService wordsService = new WordsService(pairItemsDAO);
        Word word = new Word();
        word.setId(2L);
        word.setValue("House");
        wordsService.addWordEntity(word, true);
    }

    @Test(expected = WordNotFoundException.class)
    public void editWordNotPresentInStorage() {
        WordsService wordsService = new WordsService(mockEmptyDAO);
        Word word = new Word();
        word.setId(2L);
        word.setValue("House");
        wordsService.addWordEntity(word, true);
    }

    @Test
    public void deleteWords() {
        WordsService wordsService = new WordsService(pairItemsDAO);
        Word word = new Word();
        word.setId(1L);
        word.setValue("House");
        Word wordSecondary = new Word();
        wordSecondary.setId(2L);
        wordSecondary.setValue("Tree");
        wordsService.deleteWord(word);
        wordsService.deleteWord(wordSecondary);
    }

    @Test
    @Ignore
    public void deleteNotPresentWord() {
        WordsService wordsService = new WordsService(mockEmptyDAO);
        Word word = new Word();
        word.setId(1L);
        word.setValue("House");
        wordsService.deleteWord(word);
    }

    @Test(expected = WordNotFoundException.class)
    public void emptyDataBase() {
        WordsService wordsService = new WordsService(mockEmptyDAO);
        Assert.assertEquals(new Word(1L, "House"), wordsService.getWord(3L));
    }
}
