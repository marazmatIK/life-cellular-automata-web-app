package com.nchernov.life.inject;

import com.nchernov.life.entity.Word;
import com.nchernov.life.services.EntityDAO;
import com.nchernov.life.services.WordDAO;
import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * Created by zugzug on 01.10.15.
 */
@Provider
public class EntityDAOProvider extends SingletonTypeInjectableProvider<Context, EntityDAO<Word>> {
    public EntityDAOProvider() {
        super(EntityDAO.class, new WordDAO());
    }
}
