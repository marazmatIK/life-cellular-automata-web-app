package com.nchernov.life.services;

import com.nchernov.life.entity.Word;
import com.nchernov.life.exceptions.DictionaryException;
import com.nchernov.life.exceptions.ValidationException;
import com.nchernov.life.exceptions.WordNotFoundException;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.Context;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by zugzug on 20.09.15.
 */
public class WordsService {
    @Context private EntityDAO<Word> wordEntityDAO;

    public WordsService(@Context EntityDAO<Word> wordEntityDAO) {
        this.wordEntityDAO = wordEntityDAO;
    }

    public Word getWord(Long id) throws WordNotFoundException {
        return getWordEntityFromId(wordEntityDAO, id);
    }

    public Collection<Word> findWord(String startsWith) throws WordNotFoundException {
        return wordEntityDAO.find(String.format("from Word where value LIKE '%s%s'", startsWith, "%"));
    }

    public Word deleteWord(Word word) throws WordNotFoundException {
        return wordEntityDAO.delete(word);
    }

    public Word addWordEntity(Word word, boolean replaceExisting) throws WordNotFoundException, DictionaryException, ValidationException {
        if (StringUtils.isEmpty(word.getValue())) {
            throw new ValidationException("Empty word not allowed");
        }
        if (replaceExisting) {
            if (word.getId() == null) {
                throw new ValidationException("Word id was not specified");
            }
            Word wordExisting = getWordEntityFromId(wordEntityDAO, word.getId());
            if (!wordExisting.getValue().equals(word.getValue()) &&
                    wordEntityDAO.find(String.format("from Word where VALUE = '%s'", word.getValue())).size() > 0) {
                throw new DictionaryException("Duplication error. Such a word already exists in storage.");
            }
            wordExisting.setValue(word.getValue());
            wordEntityDAO.save(wordExisting);
        } else {
            if (wordEntityDAO.find(String.format("from Word where VALUE = '%s'", word.getValue())).size() > 0) {
                throw new DictionaryException("Duplication error. Such a word already exists in storage.");
            }
            word = wordEntityDAO.save(word);
        }
        return word;
    }

    private static Word getWordEntityFromId(EntityDAO<Word> entityDAO, Long id) {
        return getFirstWordEntity(entityDAO.find(String.format("from Word where ID = '%d'", id)));
    }

    private static Word getFirstWordEntity(Collection<Word> words) throws WordNotFoundException {
        Iterator<Word> iterator = words.iterator();
        if (!iterator.hasNext()) {
            throw new WordNotFoundException();
        }
        Word word = iterator.next();
        if (word == null) {
            throw new WordNotFoundException();
        }

        return word;
    }
}