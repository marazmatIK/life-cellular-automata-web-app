package com.nchernov.life.services;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.nchernov.life.entity.Word;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by zugzug on 20.09.15.
 */
public class WordDAO implements EntityDAO<Word> {
    private final SessionFactory sessionFactory;

    private interface Action {
        Word perform(Session session, Word word);
        public Action DELETE = new Action() {
            @Override
            public Word perform(Session session, Word word) {
                session.delete(word);
                return word;
            }
        };

        public Action SAVE = new Action() {
            @Override
            public Word perform(Session session, Word word) {
                session.saveOrUpdate(word);
                return word;
            }
        };
    }

    public WordDAO() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public Word save(Word word) {
        return transaction(Action.SAVE, word);
    }

    @Override
    public Word delete(Word word) {
        return transaction(Action.DELETE, word);
    }

    @Override
    public Collection<Word> find(String query) {
        Session session = null;
        Collection<Word> words = Collections.emptyList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query queryObj = session.createQuery(query);
            List list = queryObj.list();
            session.getTransaction().commit();
            words = Collections2.transform(list, new Function() {
                @Override
                public Word apply(@Nullable Object o) {
                    return (Word) o;
                }
            });
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            return words;
        }
    }

    private Word transaction(Action action, Word word) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            synchronized (sessionFactory) {
                session.beginTransaction();
                Word wordEntity = action.perform(session, word);
                session.getTransaction().commit();
                return wordEntity;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
