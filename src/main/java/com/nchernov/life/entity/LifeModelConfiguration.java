package com.nchernov.life.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by zugzug on 03.10.15.
 */
@Entity
@Table(name = "config")
@XmlRootElement(name = "Configuration")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class LifeModelConfiguration {
    /** id of the config */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="width")
    private byte width;

    @Column(name="height")
    private byte height;

    private User user;

    @Column(name="value")
    private String configurationString;

    public byte getWidth() {
        return width;
    }

    public void setWidth(byte width) {
        this.width = width;
    }

    public byte getHeight() {
        return height;
    }

    public void setHeight(byte height) {
        this.height = height;
    }

    public String getConfigurationString() {
        return configurationString;
    }

    public void setConfigurationString(String configurationString) {
        this.configurationString = configurationString;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
