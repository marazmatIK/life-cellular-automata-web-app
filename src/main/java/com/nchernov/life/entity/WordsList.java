package com.nchernov.life.entity;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by zugzug on 28.09.15.
 */
@XmlRootElement(name = "Words")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class WordsList {
    public WordsList(){}

    public WordsList(List<Word> words){
        this.words = words;
    }

    @XmlElement(name = "Word")
    @XmlElementWrapper(name = "List")
    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    private List<Word> words;
}
