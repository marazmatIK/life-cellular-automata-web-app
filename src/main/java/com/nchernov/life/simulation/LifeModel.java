package com.nchernov.life.simulation;

import com.nchernov.life.entity.LifeModelConfiguration;

import java.util.Arrays;

/**
 * Модель данных "Жизни".
 * Поле завернуто в тороид, т.е. и левый и правый края, и верхний и нижний, являются замкнутыми.
 * При симуляции используется принцип двойной буферизации: данные берутся из главного массива mainField, после расчета
 * результат складывается во вспомогательный массив backField. По окончании расчета одного шага ссылки на эти массивы
 * меняются местами.
 * В массивах хранятся значения: 0, если клетка мертва, и 1, если жива.
 */
public class LifeModel {

    private byte[]	mainField			= null;
    private byte[]	backField			= null;

    private int		width, height;
    private int[]	neighborOffsets		= null;
    private int[][]	neighborXYOffsets	= null;

    /**
     * Инициализация модели.
     *
     * @param width ширина поля данных
     * @param height высота поля данных
     */
    public LifeModel(int width, int height) {
        init(width, height);
    }

    public LifeModel(LifeModelConfiguration lifeModelConfiguration) {
        this(lifeModelConfiguration.getWidth(), lifeModelConfiguration.getHeight(), lifeModelConfiguration);
    }

    public LifeModel(int width, int height, LifeModelConfiguration lifeModelConfiguration) {
        String configString = lifeModelConfiguration.getConfigurationString();
        if (configString.length() != width * height) {
            throw new IllegalArgumentException("Inconsistent configuration: LifeModelConfiguration cell states string has elements count not equal to whole matrix size");
        }
        init(width, height);

        for (int i = 0; i <configString.length(); i++) {
            mainField[i] = Byte.valueOf(Character.toString(configString.charAt(i)));
        }
    }

    /**
     * "constructor" code for lazy (delayed) initialization that may even not be executed because of early errors
     * @param width
     * @param height
     */
    private void init(int width, int height) {
        this.width = width;
        this.height = height;
        mainField = new byte[width * height];
        backField = new byte[width * height];
        neighborOffsets = new int[] {-width - 1, -width, -width + 1, -1, 1, width - 1, width, width + 1};
        neighborXYOffsets = new int[][] { {-1, -1}, {0, -1}, {1, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}};
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void clear() {
        Arrays.fill(mainField, (byte) 0);
    }

    public void setCell(int x, int y, byte c) {
        mainField[y * width + x] = c;
    }

    public byte getCell(int x, int y) {
        return mainField[y * width + x];
    }

    public synchronized String getCellsString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < mainField.length; i++) {
            stringBuilder.append(mainField[i]);
        }
        return stringBuilder.toString();
    }

    public synchronized byte[] getCells() {
        return mainField.clone();
    }

    public synchronized byte[][] getCellsAsMatrix() {
        byte[][] cells = new byte[height][width];
        for (int i = 0; i < height * width; i++) {
            System.out.print(" (" + i + ") " + i / (height - 1) + ";" + i % width);
            if (i % width == (width - 1)) System.out.println();
            cells[i / height][i % (width - 1)] = mainField[i];
        }
        return cells;
    }

    /**
     * Один шаг симуляции.
     */
    public synchronized void simulate() {
        // обрабатываем клетки, не касающиеся краев поля
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                int j = y * width + x;
                byte n = countNeighbors(j);
                backField[j] = simulateCell(mainField[j], n);
            }
        }

        // обрабатываем граничные клетки
        // верхняя и нижняя строки
        for (int x = 0; x < width; x++) {
            int j = width * (height - 1);
            byte n = countBorderNeighbors(x, 0);
            backField[x] = simulateCell(mainField[x], n);
            n = countBorderNeighbors(x, height - 1);
            backField[x + j] = simulateCell(mainField[x + j], n);
        }
        // крайние левый и правый столбцы
        for (int y = 1; y < height - 1; y++) {
            int j = width * y;
            byte n = countBorderNeighbors(0, y);
            backField[j] = simulateCell(mainField[j], n);
            n = countBorderNeighbors(width - 1, y);
            backField[j + width - 1] = simulateCell(mainField[j + width - 1], n);
        }

        // обмениваем поля местами
        byte[] t = mainField;
        mainField = backField;
        backField = t;
    }

    /**
     * Подсчет соседей для не касающихся краев клеток.
     *
     * @param j смещение клетки в массиве
     * @return кол-во соседей
     */
    private byte countNeighbors(int j) {
        byte n = 0;
        for (int i = 0; i < 8; i++) {
            n += mainField[j + neighborOffsets[i]];
        }
        return n;
    }

    /**
     * Подсчет соседей для граничных клеток.
     *
     * @param x
     * @param y
     * @return кол-во соседей
     */
    private byte countBorderNeighbors(int x, int y) {
        byte n = 0;
        for (int i = 0; i < 8; i++) {
            int bx = (x + neighborXYOffsets[i][0] + width) % width;
            int by = (y + neighborXYOffsets[i][1] + height) % height;
            n += mainField[by * width + bx];
        }
        return n;
    }

    /**
     * Симуляция для одной клетки.
     *
     * @param self собственное состояние клетки: 0/1
     * @param neighbors кол-во соседей
     * @return новое состояние клетки: 0/1
     */
    private byte simulateCell(byte self, byte neighbors) {
        return (byte) (self == 0 ? (neighbors == 3 ? 1 : 0) : neighbors == 2 || neighbors == 3 ? 1 : 0);
    }

    public static void main(String[] args) {
        LifeModel lifeModel = new LifeModel(4, 5);
        lifeModel.setCell(1, 3, (byte)1);
        lifeModel.setCell(2, 2, (byte)1);
        lifeModel.setCell(0, 1, (byte)1);
        byte[][] cells = lifeModel.getCellsAsMatrix();
        System.out.println();
    }
}