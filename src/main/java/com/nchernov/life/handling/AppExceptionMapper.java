package com.nchernov.life.handling;

import com.nchernov.life.entity.Error;
import com.nchernov.life.exceptions.AppException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by zugzug on 27.09.15.
 */
@Provider
public class AppExceptionMapper implements ExceptionMapper<AppException> {
    @Override
    public Response toResponse(AppException exception) {
        return Response.status(exception.getStatus())
                .entity(new Error(null, exception.getMessage()))
                .type(MediaType.APPLICATION_XML)
                .build();
    }
}
