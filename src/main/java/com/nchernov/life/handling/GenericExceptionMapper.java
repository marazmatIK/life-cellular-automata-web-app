package com.nchernov.life.handling;

import com.nchernov.life.entity.Error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by zugzug on 27.09.15.
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable ex) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new Error(Error.ERROR_CODE_SERVER_ERROR, "Request Processing Error: " + ex.getMessage() +
                        " Cause: " + (ex.getCause() == null ? "" : ex.getCause())))
                        .type(MediaType.APPLICATION_XML)
                        .build();
    }
}
