package com.nchernov.life.rest;

import com.nchernov.life.entity.Error;
import com.nchernov.life.entity.Word;
import com.nchernov.life.entity.WordsList;
import com.nchernov.life.exceptions.ValidationException;
import com.nchernov.life.exceptions.WordNotFoundException;
import com.nchernov.life.services.WordsService;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.util.ArrayList;

/**
 * Created by zugzug on 20.09.15.
 */
@Path("word")
public class WordsResource {
    private WordsService wordsService;

    public WordsResource(@Context WordsService wordsService) {
        this.wordsService = wordsService;
    }

    @GET
    @Path("{id}")
    public Response getWord(@PathParam("id") Long id) throws JAXBException {
        return Response.status(Response.Status.OK)
                .entity(wordsService.getWord(id))
                .type(MediaType.APPLICATION_XML)
                .build();
    }

    @GET
    @Path("/find/{startsWith}")
    public Response getWord(@PathParam("startsWith") String startsWith) throws JAXBException {
        if (StringUtils.isEmpty(startsWith)) {
            throw new ValidationException("Querying character sequence should not be empty.");
        }
        wordsService = getWordsService();
        return Response.status(Response.Status.OK)
                .entity(new WordsList(new ArrayList(wordsService.findWord(startsWith))))
                .type(MediaType.APPLICATION_XML)
                .build();
    }

    /**
     * Method for test purposes
     * @param wordsService
     */
    void setWordsService(WordsService wordsService)  {
        this.wordsService = wordsService;
    }

    /**
     * Method for test purposes
     */
    WordsService getWordsService() {
        return wordsService;
    }

    private Response saveWord(Word word, boolean replaceExisting) throws JAXBException {
        try {
            Word wordAdded = wordsService.addWordEntity(word, replaceExisting);
            return Response.status(Response.Status.OK)
                    .entity(wordAdded)
                    .type(MediaType.APPLICATION_XML)
                    .build();
        } catch (WordNotFoundException ex) {
            if (replaceExisting) {
                throw new WordNotFoundException(Error.WORD_NOT_FOUND_BY_ID);
            } else {
                throw ex;
            }
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response deleteWord(@PathParam("id") Long id) throws JAXBException {
        Word word = wordsService.getWord(id);
        Word wordDeleted = wordsService.deleteWord(word);
        return Response.status(Response.Status.OK)
                .entity(wordDeleted)
                .type(MediaType.APPLICATION_XML)
                .build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Response setWord(Word word) throws JAXBException {
        return saveWord(word, true);
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response addWord(Word word) throws JAXBException {
        return saveWord(word, false);
    }
}
