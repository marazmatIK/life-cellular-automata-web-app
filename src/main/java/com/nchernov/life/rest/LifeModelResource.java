package com.nchernov.life.rest;

import com.nchernov.life.entity.LifeModelConfiguration;
import com.nchernov.life.simulation.LifeModel;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by zugzug on 03.10.15.
 */
@Path("life")
public class LifeModelResource {
    static LifeModel lifeModel;

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("/start")
    public void initModel(LifeModelConfiguration configuration) {
        lifeModel = new LifeModel(configuration);
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Path("/step")
    public Response step() {
        lifeModel.simulate();
        lifeModel.getCells();
        LifeModelConfiguration lifeModelConfiguration = new LifeModelConfiguration();
        lifeModelConfiguration.setConfigurationString(lifeModel.getCellsString());
        lifeModelConfiguration.setHeight((byte)lifeModel.getHeight());
        lifeModelConfiguration.setWidth((byte)lifeModel.getWidth());
        return Response.status(Response.Status.OK)
                .entity(lifeModelConfiguration)
                .type(MediaType.APPLICATION_XML)
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Path("/save")
    public Response save() {
        //TODO: code persisting passed config into Storage
        return Response.status(Response.Status.OK)
                .entity(new LifeModelConfiguration())
                .type(MediaType.APPLICATION_XML)
                .build();
    }
}
