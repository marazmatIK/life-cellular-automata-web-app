package com.nchernov.life.pages;

import java.io.StringWriter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

@Path("/life")
public class HtmlPageResource {
    public static String LOGGER_NAME = HtmlPageResource.class.getName();

    @Context
    VelocityEngine velocity;

    public HtmlPageResource() {
    }

    @GET
    @Produces("text/html")
    public String getClichedMessage() {
        //Logger log = Logger.getLogger(LOGGER_NAME);
        if (velocity != null) {
            //log.info("Found Velocity Engine");
        } else {
            //log.error("Velocity Engine not found");
        }
        VelocityContext context = new VelocityContext();
        StringWriter sw = null;
        try {
            context.put("name", new String("Velocity"));
            Template template = velocity.getTemplate("page.vm");
            sw = new StringWriter();
            template.merge(context, sw);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
        return sw.toString();
    }
}
