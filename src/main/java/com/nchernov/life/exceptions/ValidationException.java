package com.nchernov.life.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by zugzug on 20.09.15.
 */
public class ValidationException extends AppException {
    public ValidationException(String message) {
        super(message);
    }

    public int getStatus() {
        return Response.Status.BAD_REQUEST.getStatusCode();
    }
}
