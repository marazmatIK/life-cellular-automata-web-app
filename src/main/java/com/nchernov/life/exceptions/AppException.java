package com.nchernov.life.exceptions;

import javax.ws.rs.core.Response;

/**
 * Created by zugzug on 27.09.15.
 */
public class AppException extends RuntimeException {
    int status;

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable ex) {
        super(message, ex);
    }

    public int getStatus() {
        return Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
