package com.nchernov.life.exceptions;

import javax.ws.rs.core.Response;
import com.nchernov.life.entity.Error;

/**
 * Created by zugzug on 20.09.15.
 */
public class WordNotFoundException extends AppException {
    public WordNotFoundException() {
        super(Error.WORD_NOT_FOUND);
    }

    public WordNotFoundException(String message) {
        super(message);
    }

    public WordNotFoundException(String message, Throwable ex) {
        super(message, ex);
    }

    public int getStatus() {
        return Response.Status.BAD_REQUEST.getStatusCode();
    }
}
